import json
import sys

# Script to convert SVO profile service ASCII filter definitions into
# Siril json format. The script only populates the wavelength and values
# arrays, you WILL need to edit the other metadata (name, model, channel etc.)
# yourself! Or edit it in the converter to have an easier job of batch conversion.

def read_data_file(filename):
    wavelength_values = []
    values_values = []

    with open(filename, 'r') as file:
        for line in file:
            wavelength, value = map(float, line.split())
            wavelength_values.append(wavelength)
            values_values.append(value)

    return wavelength_values, values_values

def create_json_file(wavelength_values, values_values):
    data = [
        {
            "model": "Nikon",
            "name": "Nikon",
            "type": "OSC_SENSOR",
            "dataQualityMarker": 3,
            "dataSource": "SVO Filter Profile Service",
            "manufacturer": "Nikon",
            "version": 1,
            "channel": "RED",
            "wavelength": {
                "value": wavelength_values,
                "units": "angstrom"
            },
            "values": {
                "value": values_values,
                "range": 1
            }
        }
    ]

    # Use separators to ensure all elements are on a single line
    return json.dumps(data, indent=2, separators=(',', ':'))

def main():
    if len(sys.argv) != 2:
        print("Usage: python script.py input_filename")
        sys.exit(1)

    input_filename = sys.argv[1]
    wavelength_values, values_values = read_data_file(input_filename)
    json_data = create_json_file(wavelength_values, values_values)

    print(json_data)

if __name__ == "__main__":
    main()
